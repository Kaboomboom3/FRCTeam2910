package org.frcteam2910.c2017;

public class RobotMap {
	public static final int[] DRIVETRAIN_SHIFTER = {6, 7};

	public static final int[] DRIVETRAIN_LEFT_MOTORS = {3, 4, 5};
	public static final int[] DRIVETRAIN_LEFT_ENCODER = {2, 3};

	public static final int[] DRIVETRAIN_RIGHT_MOTORS = {0, 1, 2};
	public static final int[] DRIVETRAIN_RIGHT_ENCODER = {0, 1};
}
