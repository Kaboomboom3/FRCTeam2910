package org.frcteam2910.c2017;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import org.frcteam2910.c2017.subsystems.Drivetrain;
import org.frcteam2910.common.Logger;
import org.frcteam2910.common.robot.subsystems.SubsystemManager;

public final class Robot extends TimedRobot {
	private final Drivetrain drivetrain = Drivetrain.getInstance();
	private final SubsystemManager subsystemManager = new SubsystemManager(drivetrain);

	private final OI oi = OI.getInstance();

	private final Logger logger = new Logger(Robot.class);

	@Override
	public void robotInit() {
		try {
			logger.info("Initializing robot code");

			oi.bindControls();
		} catch (Throwable t) {
			logger.error(t);
			throw t;
		}
	}

	@Override
	public void robotPeriodic() {
		subsystemManager.outputToSmartDashboard();
		subsystemManager.writeToLog();

		Scheduler.getInstance().run();
	}

	@Override
	public void autonomousInit() {
		try {
			logger.info("Beginning autonomous mode");
		} catch (Throwable t) {
			logger.error(t);
			throw t;
		}
	}

	@Override
	public void disabledInit() {
		try {
			logger.info("Disabling robot");

			subsystemManager.stop();
		} catch (Throwable t) {
			logger.error(t);
			throw t;
		}
	}

	@Override
	public void teleopInit() {
		try {
			logger.info("Beginning teleoperated mode");

			drivetrain.stop();
		} catch (Throwable t) {
			logger.error(t);
			throw t;
		}
	}
}
